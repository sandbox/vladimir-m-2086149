<?php
/**
 * @file
 * Describe hooks provided by the Multi Steps Form module.
 */

/**
 * Implements hook_multisteps_form_steps().
 *
 * $steps - An associative array describing the steps structure.
 */
function hook_multisteps_form_steps(&$steps) {
  // Define array of forms ids.
  $steps = array(
    0 => array(
      // Defile form name (form_id).
      'form' => 'multisteps_form_start_form',
    ),
    1 => array(
      'form' => 'multisteps_form_finish_form',
    ),
  );
}
